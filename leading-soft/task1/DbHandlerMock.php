<?php
/**
 * @version PHP 8.0.1
 * @author Alejandro A. Shevyakov <sindyjay@yandex.ru>
 */

namespace leadingsoft;


use PDO;
use Exception;

class DbHandlerMock
{
	private const DB_NAME     = 'test_db';
	private const DB_HOST     = 'localhost';
	private const DB_CHARSET  = 'UTF8';
	private const DB_USER     = 'test_user';
	private const DB_PASSWORD = 'test_password';

	private $dbHandler;

	/**
	 * @throws Exception
	 */
	public function __construct() {
		try {
			$this->setDbHandler(new PDO(
				sprintf(
					'mysql:dbname=%s;host=%s;charset=%s',
					self::DB_NAME,
					self::DB_HOST,
					self::DB_CHARSET
				),
				self::DB_USER,
				self::DB_PASSWORD
			));
		}
		catch (Exception $exception) {
			throw new Exception($exception->getMessage(), $exception->getCode());
		}
	}

    /**
     * MOCKUP: Test function
     *
     * @param string $sqlString
     * @return array
     */
	public function RawQuery(string $sqlString): array {
		$dbRequest = $this->getDbHandler()->prepare($sqlString);
		//$dbRequest->bindParam(':param', 'value')
		$dbRequest->execute();
		return $dbRequest->fetchAll(PDO::FETCH_ASSOC)[0];
	}



	private function setDbHandler(PDO $dbHandler): void {
		$this->dbHandler = $dbHandler;
	}

	private function getDbHandler(): PDO {
		return $this->dbHandler;
	}
}