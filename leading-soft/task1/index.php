<?php

// 1. load dependencies:
use leadingsoft\Item;

require_once 'DbHandlerMock.php';
require_once 'Item.php';

try {

    $test = new Item(0);
    $test->__set('id', 15);
    print $test->__get('status');

}
catch (Exception $exception) {
    print sprintf('The program aborted with code %s: %s', (string)$exception->getCode(), $exception->getMessage());
    die();
}