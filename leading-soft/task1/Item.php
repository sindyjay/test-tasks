<?php
/**
 * @version PHP 8.0.1
 * @author Alejandro A. Shevyakov <sindyjay@yandex.ru>
 */

namespace leadingsoft;


//use leadingsoft\DbHandlerMock as DbHandler;
use Exception;


/**
 * Class Item
 * @package leadingsoft
 */
final class Item
{

	private $id;
	private $name;
	private $status;
	private $changed;

    /**
     * Create a new Item instance
     *
     * @param int $id
     * @throws Exception
     */
    public function __construct(int $id) {
        try {
            $this->setId($id);
            $this->init();
        }
        catch (Exception $exception) {
            throw new Exception($exception->getMessage(), $exception->getCode());
        }
	}

    /**
     * Get a hidden field value by Name or throw exception if field is unknown.
     *
     * @param string $fieldName
     * @throws Exception
     */
    public function __get(string $fieldName) {
        if (isset($this->$fieldName)) {
            if ($this->$fieldName !== null) {
                return $this->$fieldName;
            }
        }
        throw new Exception(sprintf('The value of the "%s" is not set', $fieldName));
	}

    /**
     * Set the value of the hidden field (for all existing fields except ID)
     *
     * @param string $name
     * @param mixed $value
     * @throws Exception
     */
    public function __set(string $name, $value): void {
        if ($name === 'id') {
            throw new Exception('Attempt to write a read-only field');
        }
        if (isset($this->$name)) {
            if (gettype($this->name) === gettype($value)) {
                $this->$name = $value;
                //if (in_array($name, ['name', 'status'])) {
                    $this->setChanged(true);
                //}
            }
            else {
                throw new Exception(sprintf('Invalid data type for "%s" property.', $name));
            }
        }
        else {
            throw new Exception(sprintf('Unknown property "%s".', $name));
        }
    }


    /**
     * Saves the values in the database if the fields were changed from outside the class.
     *
     * @param string $name
     * @param int $status
     * @throws Exception
     */
    public function Save(string $name, int $status) {
        if ($this->getChanged() === true) {
            /*
            db = new DbHandler();
            $result = $db->RawQuery(sprintf(
                'UPDATE `objects` SET `name` = "%s", `status` = %d WHERE `someCriteria` = "someValue";',
                $name,//TODO need to use PDO->bindValue() func. for safe insert
                $status
            ));
            */
            print "saved successfully" . PHP_EOL;
            return;
        }
        throw new Exception('The fields have not been changed before.');
    }



    /**
	 * @throws Exception
	 */
	private function init() {
		try {
			//db = new DbHandler();
			// можно, конечно, хранить весь вывод в поле класса, а на данном этпе сохранять только первую или последнюю запись,
			// но это просто пример. И допустим, тут есть 'WHERE' и выборка идет только одной записи
			//$result = $db->RawQuery('SELECT `name`, `status` FROM `objects` WHERE `someCriteria` = "someValue" LIMIT 1;');
            $result = ['name' => 'test', 'status' => 1];
			if (is_null($result)) {
                throw new Exception('DB returns null result');
            }
			$this->setName($result['name']);
			$this->setStatus($result['status']);
		}
		catch (Exception $exception) {
			throw new Exception($exception->getMessage(), $exception->getCode());
		}
	}



	private function setId(int $id): void { $this->id = $id; }
	private function getId(): int { return $this->id ?? 0; }

	private function setName(string $name): void { $this->name = $name; }
	private function getName(): string { return $this->name ?? ''; }

	private function setStatus(int $status): void { $this->status = $status; }
	private function getStatus(): int { return $this->status ?? 0; }

	private function setChanged(bool $changed): void { $this->changed = $changed; }
	private function getChanged(): bool { return $this->changed ?? false; }
}