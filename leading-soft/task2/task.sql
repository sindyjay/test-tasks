-- INSTALL DB AND TABLES

-- DROP DATABASE IF EXISTS `leadingsoft`;
CREATE DATABASE IF NOT EXISTS `leadingsoft` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `leadingsoft`;

CREATE TABLE `leadingsoft`.`users` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `login` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
    `password` INT(32) NOT NULL ,
    `object_id` INT(11) NOT NULL ,
    PRIMARY KEY (`id`),
    UNIQUE (`login`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `objects` (
    `id` int(11) NOT NULL AUTO_INCREMENT ,
    `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `status` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- QUERY EXAMPLE
SELECT
    u.id AS user_id,
    u.object_id AS object_id,
    u.login,
    u.password,
    o.status,
    o.name AS object_name
FROM leadingsoft.users AS u
    INNER JOIN leadingsoft.objects As o ON u.object_id = o.id
WHERE u.object_id = 0; -- Object ID можно передавать программно. 0 здесь для примера.
